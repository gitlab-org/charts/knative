# DEPRECATED

This chart was part of GitLab's One-click app offerings for Kubernetes, to facilitate getting started with GitLab Serverless. Both One-click apps and Serverless have now been removed.

*DO NOT USE THIS CHART!** It will not work on Kubernetes 1.22 and later. Follow the latest instructions on https://knative.dev instead.

---

# Chart for Knative

This is a fork from the [Triggermesh Knative Helm chart](https://github.com/triggermesh/charts).

It installs the released manifest for knative, including Istio as [documented](https://github.com/knative/docs/blob/master/install/Knative-with-any-k8s.md)

Note that because of Istio [RBAC](https://istio.io/v1.0/docs/setup/kubernetes/helm-install/#installation-steps), you need Helm 2.10 or later. Helm 3 is supported as of `v0.10.0` of this chart.

## About this Chart

### Istio

This chart installs istio in a "lean" configuration which means it lacks the sidecar injection. This chart is generated from the [recommendation in the knative docs](https://github.com/knative/docs/blob/release-0.11/docs/install/installing-istio.md#installing-istio-without-sidecar-injection).

### Knative

The Knative chart is based on the the kubernetes yaml [provided in the documentation](https://github.com/knative/docs/blob/release-0.11/docs/install/Knative-with-any-k8s.md). It only provides the knative-serving functionality and not eventing. It is configured to download the yaml and templatize it in the cloudbuild step.

## Setup the Chart repo

```shell
helm repo add gitlab https://charts.gitlab.io/
```

or if you would like to use the latest chart from master:

```shell
git clone https://gitlab.com/gitlab-org/charts/knative.git
helm install knative
```

And update your chart repos:

```shell
helm repo update
```

## Search for knative and install

```shell
helm search knative
helm install --debug --dry-run gitlab/knative
```

If you are sure you want to do the install:

```shell
helm install gitlab/knative
```

## Support

We would love your feedback on this chart so don't hesitate to let us know what is wrong and how we could improve it, just file an [issue](https://gitlab.com/gitlab-org/charts/knative/issues/new)

## Upgrading Knative Version

Historically, this chart was compiled using a script and downloaded the knative-serving portion from https://github.com/knative/serving/releases/download/{version}/serving-post-1.14.yaml.

You can still do that!

1. Download the new version of the knative-serving yaml
1. Replace instances of `config-domain` with `config-domain-example` (Yes, this results in resources named `config-domain-example-example`. No, we shouldn't change it at this point.)
1. Replace instances of `{{` with `{{ "{{" }}`. Helm doesn't like the `{{`, but it shows up in the template.
1. Remove instances of namespace creation so they happen as a part of the knative pre-install
1. Remove any duplicate components (I guess this happens sometimes?)
1. Insert all CRDs to the `crds` folder and annotate them with

   ```yaml
   metadata:
     annotations:
       "helm.sh/hook": "crd-install"
   ```

1. Insert the new `knative-serving` YAML into `knative.yaml` leaving the two Namespace creations at the top and the last ConfigMap creation at the bottom. (There are comments to make sure you see the right spot.)

## [Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)
